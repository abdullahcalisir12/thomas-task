module.exports = {
  purge: [
    './components/**/*.{vue,js}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
   darkMode: false, // or 'media' or 'class'
   theme: {
     extend: {
      gradientColorStops: theme => ({
        ...theme('colors'),
        't-green': '#319795',
        't-blue': '#3182CE'
       }),
      colors: {
        't-green': '#319795',
        't-blue': '#3182CE'
      }
     }
   },
   variants: {
     extend: {},
   },
   plugins: [],
}